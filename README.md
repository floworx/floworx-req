# Floworx Requirement Management System

Calliope: https://floworx-req-calliope.herokuapp.com

## Status
[![Codeship](https://img.shields.io/codeship/b8ea5950-5cc9-0134-0318-066f4b004e09.svg?maxAge=2592000)]()
[![Coveralls](https://img.shields.io/coveralls/floworx/floworx-req.svg?maxAge=2592000)]()