class FrontPagesController < ApplicationController
  def home
  end

  def about
  end

  def contact
  end

  def terms
  end

  def privacy
  end
end
