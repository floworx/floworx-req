Rails.application.routes.draw do
  get 'registrations/new'

  root 'front_pages#home'
  get 'about', to: 'front_pages#about', as: :front_about
  get 'contact', to: 'front_pages#contact', as: :front_contact
  get 'terms', to: 'front_pages#terms', as: :front_terms
  get 'privacy', to: 'front_pages#privacy', as: :front_privacy

  get 'dashboard', to: 'dashboards#show', as: :dashboard

  get 'login', to: 'sessions#new', as: :login
  get 'register', to: 'registrations#new', as: :registration
end
