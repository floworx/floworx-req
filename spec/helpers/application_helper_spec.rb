RSpec.describe ApplicationHelper, type: :helper do
  describe '#full_title' do
    it 'should return a default title with no params' do
      expect(helper.full_title).to eq('Floworx')
    end

    it 'should return a title with provided params' do
      expect(helper.full_title('Test title')).to eq('Test title | Floworx')
    end
  end
end
