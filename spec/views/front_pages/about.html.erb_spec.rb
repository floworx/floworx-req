RSpec.describe 'front_pages/about.html.erb', type: :view do
  it 'should have page name' do
    render template: 'front_pages/about', layout: 'layouts/front_pages'
    expect(rendered).to have_title('About | Floworx')
  end

  it 'should have front page links' do
    render
    expect(rendered).to have_link('Home')
    expect(rendered).to have_link('Contact')
    expect(rendered).to have_link('Terms')
    expect(rendered).to have_link('Privacy')
  end
end
