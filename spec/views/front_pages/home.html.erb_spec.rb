RSpec.describe 'front_pages/home.html.erb', type: :view do
  it 'should have page name' do
    render template: 'front_pages/home', layout: 'layouts/front_pages'
    expect(rendered).to have_title('Floworx')
  end

  it 'should have front page links' do
    render
    expect(rendered).to have_link('About')
    expect(rendered).to have_link('Contact')
    expect(rendered).to have_link('Terms')
    expect(rendered).to have_link('Privacy')
  end
end
