RSpec.describe 'front_pages/privacy.html.erb', type: :view do
  it 'should have page name' do
    render template: 'front_pages/privacy', layout: 'layouts/front_pages'
    expect(rendered).to have_title('Privacy | Floworx')
  end

  it 'should have front page links' do
    render
    expect(rendered).to have_link('Home')
    expect(rendered).to have_link('About')
    expect(rendered).to have_link('Contact')
    expect(rendered).to have_link('Terms')
  end
end
