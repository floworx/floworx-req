RSpec.describe 'front_pages/terms.html.erb', type: :view do
  it 'should have page name' do
    render template: 'front_pages/terms', layout: 'layouts/front_pages'
    expect(rendered).to have_title('Terms | Floworx')
  end

  it 'should have front page links' do
    render
    expect(rendered).to have_link('Home')
    expect(rendered).to have_link('About')
    expect(rendered).to have_link('Contact')
    expect(rendered).to have_link('Privacy')
  end
end
